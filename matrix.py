import copy
import numpy
def matrixGenerator(sz):
    return numpy.random.randint(sz*sz*sz, size=(sz,sz))

def matrixRotate90(mat):
    matDim = len(mat)
    for i in range(1,matDim+1,2):
        if i is not 1:
            #Each index layer represents the starting point of 4 sub-arrays in a layer
            diag1 =(matDim-i)/2
            diag2 = ((matDim-i)/2)+i-1
            indexLayer1, indexLayer2, indexLayer3, indexLayer4 =[diag1,diag1], [diag1, diag2],[diag2, diag2], [diag2, diag1]
            #for loop that completes rotation for a single layer
            for j in range(0,i-1):
                swap(mat, indexLayer1, indexLayer2, indexLayer3, indexLayer4)
                indexLayer1[1]+=1
                indexLayer2[0]+=1
                indexLayer3[1]-=1
                indexLayer4[0]-=1
def swap(mat, i, j, k, l):
    #Swaps elements at i, j, k, l cyclically (i->j, j->k, k->l, l->i)
    a,b,c,d = mat[i[0],i[1]],mat[j[0],j[1]],mat[k[0],k[1]],mat[l[0],l[1]]
    mat[j[0],j[1]], mat[k[0],k[1]], mat[l[0],l[1]], mat[i[0],i[1]] = a,b,c,d

def matrixRotate180(mat):
    [matrixRotate90(mat) for _ in range(2)]

def matrixRotate270(mat):
    [matrixRotate90(mat) for _ in range(3)]

def matrixRotate360(mat):
    [matrixRotate90(mat) for _ in range(4)]
def matrixEqualityChecker(mat1, mat2):
    for i in range(0, len(mat1)):
        for j in range(0, len(mat1)):
            if mat1[i,j] != mat2[i,j]:
                return False
    return True

#Simple testing harness
def tester(matSz):
    mat = matrixGenerator(matSz)
    tempMat = copy.deepcopy(mat)
    print "Matrix dimension is: ", matSz, " x ", matSz
    #print "Before rotation"
    #print mat
    matrixRotate360(mat)
    #print "After rotation"
    #print mat
    if matrixEqualityChecker(mat, tempMat):
        print "------Successful 360 degree rotation completed-------"
        return True
    else:
        print "------360 degree rotation unsuccessful-------"
        return False
for i in range(2,10000):
    if tester(i) is not True:
        print "Please fix bug to continue testing"
        break

