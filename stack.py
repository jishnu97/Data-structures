class Stack:
    def __init__(self):
        self.items=[]
        self.size=0

    def push(self, item):
        self.items.append(item)
        self.size+=1
    def pop(self):
        self.size-=1
        return self.items.pop()
    def peek(self):
        return self.items[-1]

    def printStack(self):
        print "Size is:", self.size
        print "Stack: ", self.items


s = Stack()
s.push(5)
s.push(10)
s.push(15)
print s.pop()

s.printStack()
